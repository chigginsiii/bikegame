#!/usr/bin/perl
use strict;
use lib qw(../lib);

use BikeGame::Controller::Dispatch;
BikeGame::Controller::Dispatch->dispatch();
