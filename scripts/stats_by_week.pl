#!/usr/bin/perl -w

use strict;
use lib '../lib';
use Getopt::Long;
use DBI;
use BikeGame::Config;
use DateTime;

my ($name);
GetOptions(
  'name|n:s' => \$name,
);

my $prog = File::Basename::basename($0);
my $USAGE = "$prog: show weekly totals of rides and miles by player and ride-type

--name | -n       name of the player
";

unless ($name) {
    die $USAGE;
}

# get db handle
my $db_cfg = BikeGame::Config->get('db');
my $dbh = DBI->connect( @{$db_cfg}{qw(dbi_dsn db_user db_pass)}, {RaiseError => 1} );

# get the player id
my $row = $dbh->selectrow_arrayref("SELECT player_id FROM player WHERE name = '$name'");
die "Could not find stats for player '$name'" unless $row;
my $player_id = $row->[0];

# get the number of rides, number of miles, ride_type week number for this player
# group by ride_type, week_number
# sort by week_number ascending
my $rides_by_week_sql = q{
SELECT
  IF(r.ride_ref, ref.distance, r.distance),
  IF(r.ride_ref, ref.climb, r.climb),
  rr.ride_type,
  YEAR(r.date) as year,
  WEEKOFYEAR(r.date) as week,
  MONTH(r.date),
  DAYOFMONTH(r.date)
FROM
  ride r
  LEFT OUTER JOIN ride_ref ref ON ( r.ride_ref = ref.ride_ref_id )
  LEFT JOIN ride_record rr ON ( r.ride_record = rr.ride_record_id )
WHERE
  r.player = ?
};
my $sth = $dbh->prepare($rides_by_week_sql);
my $num_rows = $sth->execute($player_id);

my ( $dist, $climb, $type, $year, $week, $month, $dom );
$sth->bind_columns( \$dist, \$climb, \$type, \$year, \$week, \$month, \$dom );

my $record = {};
my $types = {};
my $dates = {};
while ( my $row = $sth->fetch() ) {

    # keep a list of types here
    $types->{$type} = 1;

    # keep a list of date ranges here
    $dates->{$year}->{$week}->{raw}->{$month}->{$dom} = 1;

    # initialize record structures
    $record->{$year} = {} unless $record->{$year};
    $record->{$year}->{$week} = { total => {} } unless $record->{$year}->{$week};
    $record->{$year}->{$week}->{$type} = {} unless $record->{$year}->{$week}->{$type};
    # rides distance and climbs by type and total
    unless ( $record->{$year}->{$week}->{$type} ) {
        $record->{$year}->{$week}->{$type}->{rides} = 0;
        $record->{$year}->{$week}->{$type}->{dist} = 0;
        $record->{$year}->{$week}->{$type}->{climb} = 0;
    }
    unless ( $record->{$year}->{$week}->{total} ) {
        $record->{$year}->{$week}->{total}->{rides} = 0;
        $record->{$year}->{$week}->{total}->{dist} = 0;
        $record->{$year}->{$week}->{total}->{climb} = 0;
    }
    # increment everything!
    $record->{$year}->{$week}->{total}->{rides}++;
    $record->{$year}->{$week}->{$type}->{rides}++;
    $record->{$year}->{$week}->{total}->{dist} += $dist;
    $record->{$year}->{$week}->{$type}->{dist} += $dist;
    $record->{$year}->{$week}->{total}->{climb} += $climb;
    $record->{$year}->{$week}->{$type}->{climb} += $climb;
}
foreach my $year ( keys %$dates) {
    foreach my $week ( keys %{ $dates->{$year} } ) {
        my $week_hash = $dates->{$year}->{$week}->{raw};
        my @months = sort { $a <=> $b } keys %$week_hash;
        # week might span two months
        my ($lm,$hm) = @months;
        $hm ||= $lm;
        # get the lowest day of the low month
        my @l_days = sort { $a <=> $b } keys %{ $week_hash->{$lm} };
        my $l_day = shift(@l_days);
        # get the highest day of the highest mont
        my @h_days = sort { $a <=> $b } keys %{ $week_hash->{$hm} };
        my $h_day = pop(@h_days);
        # make the range
        $dates->{$year}->{$week}->{range} = "$lm/$l_day-$hm/$h_day";
    }
}


#
# Okay, lay it out:
#
# week     MTB                Road               Total
#          rides dist climb   rides dist climb   rides dist climb
# ---------------------------------------------------------------
# 1/1-1/7  13    1345 35252   n/a                13    1345 35252
#
#
# things we need to know:
# - the beginning and end month/day of each year/week (we could just cap this at 11)
# - columns: the widest widths of the week header / values (let's cap at: 5, 5, and 7)
# - columns: the widest widths of each of the ride/dist/climbs for each type (let's use the above to come up with 5/5/7)
# - columns: the widest combined width to make the big heads (let's say 17 plus the spaces)
# 11 | ( 2 | 5 | 2 | 5 | 2 | 7 ) foreach type | ( 2 | 5 | 2 | 5 | 2 | 7 ) for total
my @types = sort keys %$types;
push @types, 'total';
my $num_types = scalar( @types );

my $format = "%-11s" . ( "  %-5s  %-8s  %-7s" x $num_types ) . "\n";
my $head_fmt = "%-11s" . ( "  %-24s" x $num_types ) . "\n";
my $line_len = 11 + ( 26 * $num_types );
my $line = ( '=' x $line_len ) . "\n";

printf($head_fmt, 'week', @types);
printf($format, '', map { qw(rides dist climb) } @types);
print "$line";
foreach my $year ( sort keys %$record ) {
    foreach my $week ( sort { $a <=> $b } keys %{ $record->{$year} } ) {
        my $stats = $record->{$year}->{$week};
        my $date_range = $dates->{$year}->{$week}->{range};
        printf($format, $date_range, map { map { $_ || ''} @{$stats->{$_}}{qw(rides dist climb)} } @types);
    }
}

#
# compile the numbers for totals and averages
#
my $num_weeks = 0;
my $totals = { map { $_ => {rides => 0, dist => 0, climb => 0} } @types };
foreach my $year ( values %$record ) {
    # keys of year are weeks
    foreach my $week ( values %$year ) {
        $num_weeks++;
        foreach my $type ( keys %$week ) {
            foreach my $stat_name ( qw(rides dist climb) ) {
                $totals->{$type}->{$stat_name} += $week->{$type}->{$stat_name} || 0;
            }
        }
    }
}
print "$line";
printf($format, 'TOTALS', map { my $type_name = $_; map { $totals->{$type_name}->{$_} } qw(rides dist climb) } @types);

my $avgs = { map { $_ => {rides => 0, dist => 0, climb => 0} } @types };
foreach my $type ( @types ) {
    foreach my $stat_name ( qw(rides dist) ) {
        $avgs->{$type}->{$stat_name} = sprintf("%.2f", ( $totals->{$type}->{$stat_name} / $num_weeks ) );
    }
    # climb doesn't need floats
    $avgs->{$type}->{'climb'} = sprintf("%i", ( $totals->{$type}->{'climb'} / $num_weeks ) );
}
printf($format, 'AVGS', map { my $type_name = $_; map { $avgs->{$type_name}->{$_} } qw(rides dist climb) } @types);
