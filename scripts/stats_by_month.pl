#!/usr/bin/perl -w

use strict;
use lib '../lib';
use Getopt::Long;
use DBI;
use BikeGame::Config;
use DateTime;

my ($name);
GetOptions(
  'name|n:s' => \$name,
);

my $prog = File::Basename::basename($0);
my $USAGE = "$prog: show weekly totals of rides and miles by player and ride-type

--name | -n       name of the player
";

unless ($name) {
    die $USAGE;
}

# get db handle
my $db_cfg = BikeGame::Config->get('db');
my $dbh = DBI->connect( @{$db_cfg}{qw(dbi_dsn db_user db_pass)}, {RaiseError => 1} );

# get the player id
my $row = $dbh->selectrow_arrayref("SELECT player_id FROM player WHERE name = '$name'");
die "Could not find stats for player '$name'" unless $row;
my $player_id = $row->[0];

# get the number of rides, number of miles, ride_type week number for this player
# group by ride_type, week_number
# sort by week_number ascending
my $rides_by_week_sql = q{
SELECT
  IF(r.ride_ref, ref.distance, r.distance),
  IF(r.ride_ref, ref.climb, r.climb),
  rr.ride_type,
  YEAR(r.date) as year,
  MONTH(r.date),
  DAYOFMONTH(r.date)
FROM
  ride r
  LEFT OUTER JOIN ride_ref ref ON ( r.ride_ref = ref.ride_ref_id )
  LEFT JOIN ride_record rr ON ( r.ride_record = rr.ride_record_id )
WHERE
  r.player = ?
};
my $sth = $dbh->prepare($rides_by_week_sql);
my $num_rows = $sth->execute($player_id);

my ( $dist, $climb, $type, $year, $month, $dom );
$sth->bind_columns( \$dist, \$climb, \$type, \$year, \$month, \$dom );

my $record = {};
my $types = {};
my $dates = {};
while ( my $row = $sth->fetch() ) {

    # keep a list of types here
    $types->{$type} = 1;
    # keep list of dates here
    $dates->{$year}->{$month}->{raw}->{$dom} = 1;

    # initialize record structures
    $record->{$year} = {} unless $record->{$year};
    $record->{$year}->{$month} = { total => {} } unless $record->{$year}->{$month};
    $record->{$year}->{$month}->{$type} = {} unless $record->{$year}->{$month}->{$type};
    # rides distance and climbs by type and total
    unless ( $record->{$year}->{$month}->{$type} ) {
        $record->{$year}->{$month}->{$type}->{rides} = 0;
        $record->{$year}->{$month}->{$type}->{dist} = 0;
        $record->{$year}->{$month}->{$type}->{climb} = 0;
    }
    unless ( $record->{$year}->{$month}->{total} ) {
        $record->{$year}->{$month}->{total}->{rides} = 0;
        $record->{$year}->{$month}->{total}->{dist} = 0;
        $record->{$year}->{$month}->{total}->{climb} = 0;
    }
    # increment everything!
    $record->{$year}->{$month}->{total}->{rides}++;
    $record->{$year}->{$month}->{$type}->{rides}++;
    $record->{$year}->{$month}->{total}->{dist} += $dist;
    $record->{$year}->{$month}->{$type}->{dist} += $dist;
    $record->{$year}->{$month}->{total}->{climb} += $climb;
    $record->{$year}->{$month}->{$type}->{climb} += $climb;
}
foreach my $year ( keys %$dates) {
    foreach my $month ( keys %{ $dates->{$year} } ) {
        my $month_hash = $dates->{$year}->{$month}->{raw};
        my @days = sort { $a <=> $b } keys %{ $month_hash };
        my $l_day = $days[0];
        my $h_day = $days[$#days];
        # make the range
        $dates->{$year}->{$month}->{range} = "$month/$l_day-$month/$h_day";
    }
}


#
# Okay, lay it out:
#
# week     MTB                Road               Total
#          rides dist climb   rides dist climb   rides dist climb
# ---------------------------------------------------------------
# 1/1-1/7  13    1345 35252   n/a                13    1345 35252
#
#
# things we need to know:
# - the beginning and end month/day of each year/week (we could just cap this at 11)
# - columns: the widest widths of the week header / values (let's cap at: 5, 5, and 7)
# - columns: the widest widths of each of the ride/dist/climbs for each type (let's use the above to come up with 5/5/7)
# - columns: the widest combined width to make the big heads (let's say 17 plus the spaces)
# 11 | ( 2 | 5 | 2 | 5 | 2 | 7 ) foreach type | ( 2 | 5 | 2 | 5 | 2 | 7 ) for total
my @types = sort keys %$types;
push @types, 'total';
my $num_types = scalar( @types );

my $format = "%-11s" . ( "  %-5s  %-8s  %-7s" x $num_types ) . "\n";
my $head_fmt = "%-11s" . ( "  %-24s" x $num_types ) . "\n";
my $line_len = 11 + ( 26 * $num_types );
my $line = ( '=' x $line_len ) . "\n";

printf($head_fmt, 'month', @types);
printf($format, '', map { qw(rides dist climb) } @types);
print "$line";
foreach my $year ( sort keys %$record ) {
    foreach my $month ( sort { $a <=> $b } keys %{ $record->{$year} } ) {
        my $stats = $record->{$year}->{$month};
        my $date_range = $dates->{$year}->{$month}->{range};
        printf($format, $date_range, map {
            my $s = $stats->{$_};
            ( $s->{rides} ? sprintf("%i",$s->{rides}) :  '',
              $s->{dist}  ? sprintf("%.2f",$s->{dist}) : '',
              $s->{climb} ? sprintf("%i", $s->{climb}) : ''
            )
        } @types);
    }
}

#
# compile the numbers for totals and averages
#
my $num_months = 0;
my $totals = { map { $_ => {rides => 0, dist => 0, climb => 0} } @types };
foreach my $year ( values %$record ) {
    # keys of year are months
    foreach my $month ( values %$year ) {
        $num_months++;
        foreach my $type ( keys %$month ) {
            foreach my $stat_name ( qw(rides dist climb) ) {
                $totals->{$type}->{$stat_name} += $month->{$type}->{$stat_name} || 0;
            }
        }
    }
}
print "$line";
printf($format, 'TOTALS', map { my $type_name = $_; map { $totals->{$type_name}->{$_} } qw(rides dist climb) } @types);

my $avgs = { map { $_ => {rides => 0, dist => 0, climb => 0} } @types };
foreach my $type ( @types ) {
    foreach my $stat_name ( qw(rides dist) ) {
        $avgs->{$type}->{$stat_name} = sprintf("%.2f", ( $totals->{$type}->{$stat_name} / $num_months ) );
    }
    # climb doesn't need floats
    $avgs->{$type}->{'climb'} = sprintf("%i", ( $totals->{$type}->{'climb'} / $num_months ) );
}
printf($format, 'AVGS', map { my $type_name = $_; map { $avgs->{$type_name}->{$_} } qw(rides dist climb) } @types);
